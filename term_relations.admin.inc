<?php

/**
 * @file
 * Administration code including private implementations of hooks.
 */

/**
 * Administration settings for term relations.
 */
function term_relations_admin_settings() {
  // this setting should be elsewhere
  $form['term_relations_directional'] = array(
    '#type' => 'checkbox',
    '#title' => t('Treat term relationships as directed.'),
    '#default_value' => variable_get('term_relations_directional', FALSE),
    '#description' => t('If you have a need for directed term relationships, enable this option. When disabled, the direction of term relationships may not be preserved.'),
  );

  $handlers = _term_relations_get_handlers();
  if(empty($handlers)) {
    drupal_set_message('You need to an enable at least one term relation handler - e.g. core.', 'warning', FALSE);
  }
  else {
    $handler_ids = array_keys($handlers);

    $options = array();
    foreach($handlers as $id => $handler) {
      $options[$id] = $handler['title'];
    }

    // should default to the first available handler.
    $form['term_relations_handler'] = array(
      '#type' => 'select',
      '#title' => t('Default related term storage'),
      '#default_value' => variable_get('term_relations_handler', $handler_ids[0]),
      '#options' => $options,
      // '#description' => t('If you have a need for directed term relationships, enable this option. When disabled, the direction of term relationships may not be preserved.'),
    );
  }

  return system_settings_form($form);
}
