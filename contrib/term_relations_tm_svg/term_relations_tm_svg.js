(function ($) {

Drupal.behaviors.termRelationsTMSVG = {
  attach: function(context, settings) {
    if(settings.termRelationsTM && $(context).is('#taxonomy-manager-double-tree-form')) {
      var tid = null;
      var tree1;
      var tree2;
      if(settings.termRelationsTM.direction == 'right') {
        tree1 = Drupal.settings.taxonomytree.tree[0];
        tree2 = Drupal.settings.taxonomytree.tree[1];
      }
      else {
        tree1 = Drupal.settings.taxonomytree.tree[1];
        tree2 = Drupal.settings.taxonomytree.tree[0];
      }


      Drupal.showRelations(tree1, tree2, settings);

      // get rid of these because they get in the way...
      $('div.term-line', tree1.ul).unbind('mouseover').unbind('mouseout');
      $('div.term-line', tree2.ul).unbind('mouseover').unbind('mouseout');
      $('.active-relation', tree1.ul).closest('li').find('.has-related-flag').each(function() {$(this).once('term-relations-tm-svg', function() {
        Drupal.tree2 = tree2;
        Drupal.attachRelationsHover(this, tree1, tree2, settings);
      })});
    }
  }
}

Drupal.showRelations = function(tree1, tree2, settings) {
  $('#taxonomy-manager-double-tree-form').parent().append('<div id="svg-container"></div>');
  $('#svg-container').svg({settings: {style: "position: absolute; left: 0; top: 0; width: 100%; height: 100%; z-index: 2;"}})
  .click(function() {
    $('.active-relation', tree1.ul).closest('li').find('.has-related-flag').removeClass('term-relations-tm-svg-processed');
    var svg = $(this).svg('destroy');
  });

  var svg = $('#svg-container').svg('get');
  Drupal.initialiseSVGContainer(svg);

  var tid = null;

  for(tid in settings.termRelationsTM.relations) {
    relationLines = Drupal.getRelationLines(tree1, tree2, tid, settings.termRelationsTM.relations[tid], settings.termRelationsTM.direction);
    Drupal.addRelationLines(svg, relationLines.lines, 0, 0);
  }
}

Drupal.initialiseSVGContainer = function(svg) {
  defs = svg.defs();

  var triangle = svg.marker(defs, 'Triangle', 10, 5, 8, 6, 'auto', {
    viewBox: "0 0 10 10",
    markerUnits: "strokeWidth"
  });
  var path = svg.createPath();
  svg.path(triangle, path.move(0,0).line([[10, 5], [0, 10]]).close(), {stroke: '#999999'});

  var r2lTriangle = svg.marker(defs, 'r2lTriangle', 0, 5, 8, 6, 'auto', {
    viewBox: "0 0 10 10",
    markerUnits: "strokeWidth"
  });
  var path = svg.createPath();
  svg.path(r2lTriangle, path.move(10,0).line([[0, 5], [10, 10]]).close(), {stroke: '#999999'});
}

// get the relation lines - everything needed to render - svg agnostic
// get info first so we can determine size of svg view required.
Drupal.getRelationLines = function (tree1, tree2, tid, tidRelations, treeDirection) {
  var leftTermLink = $('> .term-line > .form-item > label', tree1.getLi(tid));
  $(leftTermLink).addClass('active-relation');
  var startPosition = $(leftTermLink).position();
  var startX = startPosition.left+(treeDirection == 'right' ? $(leftTermLink).width() : 0);
  var startY = startPosition.top+($(leftTermLink).height()/2);

  // initialise return object
  var renderInfo = {minX: startX, minY: startY, maxX: startX, maxY: startY, lines: []};

  for(direction in tidRelations) {
    var rterms = tidRelations[direction];
    for(rtid in rterms) {
      // related tids
      var termLink = $('> .term-line > .form-item > label', tree2.getLi(rtid));
      $(termLink).addClass('active-relation');
      var endPosition = $(termLink).position();
      var endX = endPosition.left+(treeDirection == 'right' ? 0 : $(termLink).width());
      var endY = endPosition.top+($(termLink).height()/2);
      if(termLink != "") {
        var line = {startX: startX, startY: startY, endX: endX, endY: endY, direction: direction};
        renderInfo.lines.push(line);
      }

      // update limits
      renderInfo.minX = endX < renderInfo.minX ? endX : renderInfo.minX;
      renderInfo.minY = endY < renderInfo.minY ? endY : renderInfo.minY;
      renderInfo.maxX = endX > renderInfo.maxX ? endX : renderInfo.maxX;
      renderInfo.maxY = endY > renderInfo.maxY ? endY : renderInfo.maxY;
    }
  }

  // spread
  if(renderInfo.lines.length > 1) {
    var spacing = $(leftTermLink).height()/(renderInfo.lines.length+1);
    // sort by vertical position
    renderInfo.lines.sort(function(a, b) {return a.endY - b.endY;});
    // now add a bit of spacing
    for (var i = 0; i < renderInfo.lines.length; i++) {
      renderInfo.lines[i].startY = startPosition.top + (spacing*(i+1));
    }
  }

  return renderInfo;
}

Drupal.addRelationLines = function(svg, lines, leftOffset, topOffset) {
  // add lines to svg object
  for (var i = 0; i < lines.length; i++) {
    var line = lines[i];
    var lineProps = {stroke: '#999999', strokeWidth : 1};
    // add arrows.
    if(line.direction == 'l2r') {
      lineProps.markerEnd = "url(#Triangle)";
    }
    else if(line.direction == 'r2l') {
      lineProps.markerStart = "url(#r2lTriangle)";
    }
    else {
      lineProps.markerStart = "url(#r2lTriangle)";
      lineProps.markerEnd = "url(#Triangle)";
    }
    svg.line((line.startX)-leftOffset, (line.startY)-topOffset, (line.endX)-leftOffset, (line.endY)-topOffset, lineProps);
  }
}

Drupal.relationsHoverIn = function(e) {
  var tree1;
  var tree2;
  if(Drupal.settings.termRelationsTM.direction == 'right') {
    tree1 = Drupal.settings.taxonomytree.tree[0];
    tree2 = Drupal.settings.taxonomytree.tree[1];
  }
  else {
    tree1 = Drupal.settings.taxonomytree.tree[1];
    tree2 = Drupal.settings.taxonomytree.tree[0];
  }
  // get the tid and get the relationship lines
  var liElement = $(this).closest('li');
  var tid = Drupal.getTermId(liElement);

  var relationLines = Drupal.getRelationLines(tree1, tree2, tid, Drupal.settings.termRelationsTM.relations[tid], Drupal.settings.termRelationsTM.direction);

  // set offsets to line limits
  var leftOffset = relationLines.minX;
  var width = relationLines.maxX - relationLines.minX;
  var topOffset = relationLines.minY-10;
  var height = relationLines.maxY - relationLines.minY+20;

  // create the svg container and initialise
  $('#taxonomy-manager-double-tree-form').parent().append('<div id="svg-container"></div>');
  $('#svg-container').svg({settings: {style: "position: absolute; left: "+leftOffset+"px; top: "+topOffset+"px; width: "+width+"px; height: "+height+"px; z-index: 2;"}});
  var svg = $('#svg-container').svg('get');
  Drupal.initialiseSVGContainer(svg);

  Drupal.addRelationLines(svg, relationLines.lines, leftOffset, topOffset);

  return TRUE;
}

Drupal.relationsHoverOut = function(e) {
  $('#svg-container').svg('destroy');
  $('#svg-container').remove();
  return TRUE;
}


Drupal.attachRelationsHover = function(context, tree1, tree2, settings) {
  var context = context;
  var settings = settings;

  $(context).hover(Drupal.relationsHoverIn, Drupal.relationsHoverOut);
}

})(jQuery);
