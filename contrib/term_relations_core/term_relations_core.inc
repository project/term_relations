<?php

/**
 * @file
 * Load on demand include file containing 'private' implementations of
 * taxonomy and taxonomy form hooks.
 */

/**
 * 'Private' implementation of hook_form_FROM_ID_alter() for taxonomy_form_term.
 */
function _term_relations_core_form_taxonomy_form_term_alter(&$form, &$form_state) {
  $term = $form['#term'];
  $vocabulary = $form['#vocabulary'];

  $tree = taxonomy_get_tree($vocabulary->vid);
  $options = array('<' . t('root') . '>');

  foreach ($tree as $item) {
    $options[$item->tid] = str_repeat('-', $item->depth) . $item->name;
  }

  // get the relations - as set by implementation of hook_taxonomy_term_load().
  $related_tids = !empty($term['relations']) ? array_keys($term['relations']) : array();

  $form['relations']['relations'] = array(
    '#type' => 'select',
    '#title' => t('Related terms'),
    '#options' => $options,
    '#default_value' => $related_tids,
    '#multiple' => TRUE,
  );
}

/**
 * Implementation of term relations handler. Stores relations in taxonomy_term_data.
 */
class CoreTermRelationsHandler implements ITermRelationsHandler {
  public function getRelatedTerms($tids, $key = 'tid', $vid = 'all', $direction = NULL) {
    if ($tids && !is_array($tids)) {
      $tids = array($tids);
    }

    if($tids) {
      $query = db_select('taxonomy_term_data', 'rt');
      $query->innerJoin('taxonomy_term_data', 't', '1 = 1');
      $query->leftJoin('taxonomy_term_relation', 'tr_l2r', 'tr_l2r.tid1 = t.tid AND tr_l2r.tid2 = rt.tid');
      $query->leftJoin('taxonomy_term_relation', 'tr_r2l', 'tr_r2l.tid2 = t.tid AND tr_r2l.tid1 = rt.tid');

      // just need the tid
      $query->addField('t', 'tid', 'tid');
      $query->addField('rt', 'tid', 'rtid');
      $query->addExpression('tr_l2r.tid2 IS NOT NULL', 'l2r');
      $query->addExpression('tr_r2l.tid1 IS NOT NULL', 'r2l');

      $query->condition(db_or()->isNotNull('tr_l2r.tid2')->isNotNull('tr_r2l.tid1'));

      if($tids) {
        // drive query from selected tids
        $query->condition('t.tid', $tids, 'IN');
      }

      if (!empty($vid) && is_numeric($vid)) {
        // add vid filter
        $query->condition('rt.vid', $vid);
      }

      $query->addTag('term_access');

      // add order by clause
      $query->orderBy('t.tid');
      $query->orderBy('rt.weight');
      $query->orderBy('rt.name');

      $records = $query->execute()->fetchAll();
      return $records;
    }
    else {
      return array();
    }
  }

  public function setRelatedTerms($tid, $related_tids, $direction = NULL) {
    $term = taxonomy_term_load($tid);
    $term->relations = $related_tids;
    taxonomy_term_save($term);
  }

  public function load($terms) {
    if(!empty($terms)) {
      $tids = array_keys($terms);

      // get relationships
      $records = $this->getRelatedTerms($tids);

      // update terms
      if (term_relations_is_directed()) {
        foreach ($records as $record) {
          if ($record->l2r) {
            if (!isset($terms[$record->tid]->relations)) {
              $terms[$record->tid]->relations = array();
            }
            $terms[$record->tid]->relations[$record->rtid] = $record->rtid;
          }
          if ($record->r2l) {
            if (!isset($terms[$record->tid]->related_by)) {
              $terms[$record->tid]->related_by = array();
            }
            $terms[$record->tid]->related_by[$record->rtid] = $record->rtid;
          }
        }
      }
      else {
        foreach ($records as $record) {
          if (!isset($terms[$record->tid]->relations)) {
            $terms[$record->tid]->relations = array();
          }

          $terms[$record->tid]->relations[$record->rtid] = $record->rtid;
        }
      }
    }
  }

  public function insert($term) {
    if (isset($term->relations)) {
      if (!is_array($term->relations)) {
        $term->relations = array($term->relations);
      }
      $query = db_insert('taxonomy_term_relation')
        ->fields(array('tid1', 'tid2'));
      foreach ($term->relations as $related_tid) {
        $query->values(array(
          'tid1' => $term->tid,
          'tid2' => $related_tid
        ));
      }
      $query->execute();

      // Reset the taxonomy term static variables.
      taxonomy_terms_static_reset();
    }
  }

  public function update($term) {
    if (isset($term->relations)) {
      if(term_relations_is_directed()) {
        // only delete this relationships matching tid1
        db_delete('taxonomy_term_relation')
          ->condition('tid1', $term->tid)
          ->execute();
      }
      else {
        // delete relationships matching both tid1 and tid2
        db_delete('taxonomy_term_relation')
          ->condition(db_or()->condition('tid1', $term->tid)->condition('tid2', $term->tid))
          ->execute();
      }

      if (!is_array($term->relations)) {
        $term->relations = array($term->relations);
      }
      $query = db_insert('taxonomy_term_relation')
        ->fields(array('tid1', 'tid2'));
      foreach ($term->relations as $related_tid) {
        $query->values(array(
          'tid1' => $term->tid,
          'tid2' => $related_tid
        ));
      }
      $query->execute();

      // Reset the taxonomy term static variables.
      taxonomy_terms_static_reset();
    }
  }
}

