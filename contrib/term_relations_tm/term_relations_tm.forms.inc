<?php

require_once 'term_relations_tm.inc';

/**
 * @file
 * Collects all the taxonomy_mananger form alter and ajax handler functions into separate file for
 * easier maintenance.
 *
 * Double tree form defines operations as AJAX-ified form elements. To add ops,
 * we can just add form elements and use attachBehaviors to do any client side
 * processing.
 */

/**
 * 'Private' implementation of hook_form_FORM_ID_alter() for form id 'taxonomy_manager_double_tree_form':
 * - Adds double tree operations to relate terms and show relationships.
 * 1. Terms to expand i.e. related terms are set to the right/left_tree_expand_terms in submit handler.
 * 2. Ajax callback calls taxonomy_manager_double_tree_ajax_callback - which calls drupal render on left and/or right taxonomy_manager_tree element.
 * 3. This module replaces #process callacks of taxonomy_manager_tree elements to a) handler multiple terms expansion and b) add relationship information to terms in the tree.
 */
function _term_relations_tm_form_taxonomy_manager_double_tree_form_alter(&$form, &$form_state) {
  // double tree form bypasses normal 'taxonomy_manager_form' build workflow
  // so call our alter hook explicitly here.
  _term_relations_tm_form_taxonomy_manager_form_alter($form, $form_state);

  if (isset($form_state['values']['double_tree_values']['left_tree_expand_terms']) && count($form_state['values']['double_tree_values']['left_tree_expand_terms'])) {
    // replace expand term that was shifted off
    array_unshift($form_state['values']['double_tree_values']['left_tree_expand_terms'], $form['taxonomy']['manager']['tree']['#term_to_expand']);
    $form['taxonomy']['manager']['tree']['#term_to_expand'] = $form_state['values']['double_tree_values']['left_tree_expand_terms']; //take the first value to expand in the tree, at the moment only one term can be expanded
  }

  if (isset($form_state['values']['double_tree_values']['right_tree_expand_terms']) && count($form_state['values']['double_tree_values']['right_tree_expand_terms'])) {
    // replace expand term that was shifted off
    array_unshift($form_state['values']['double_tree_values']['right_tree_expand_terms'], $form['taxonomy2']['manager']['tree']['#term_to_expand']);
    $form['taxonomy2']['manager']['tree']['#term_to_expand'] = $form_state['values']['double_tree_values']['right_tree_expand_terms']; //take the first value to expand in the tree, at the moment only one term can be expanded
  }

  if (!variable_get('term_relations_tm_disable_relationships_extra', 0)) {
    // use our own version of element process function and add relationships decorator
    $form['taxonomy2']['manager']['tree']['#process'] = array(
      // @todo - if patch http://drupal.org/node/980556 is committed revert to standard process
      // function taxonomy_manager_tree_process_elements.
      'term_relations_tm_tree_process_elements',
      'term_relations_tm_tree_process_elements_add_relationships',
    );

    $form['taxonomy']['manager']['tree']['#process'] = array(
      // @todo - if patch http://drupal.org/node/980556 is committed revert to standard process
      // function taxonomy_manager_tree_process_elements.
      'term_relations_tm_tree_process_elements',
      'term_relations_tm_tree_process_elements_add_relationships',
    );

    $module_path = drupal_get_path('module', 'term_relations_tm');

    $vocab1 = $form_state['build_info']['args'][0];
    $vocab2 = $form_state['build_info']['args'][1];
    $vid1 = $vocab1->vid;
    $vid2 = $vocab2->vid;

    if ($supported_directionality = _term_relations_tm_get_supported_directions($vid1, $vid2)) {
      // add directed relationship operations - depending on what's allowed
      if ($supported_directionality == 'l2r' || $supported_directionality == 'bi') {
        // Relations buttons
        $form['double-tree']['operations']['relate_right'] = $form['double-tree']['operations']['move_right'];
        $form['double-tree']['operations']['show_relations_right'] = $form['double-tree']['operations']['move_right'];

        $form['double-tree']['operations']['relate_right']['#attributes']['title'] = t('Relate right.');
        $form['double-tree']['operations']['relate_right']['#src'] = $module_path ."/images/add-relationships-right.png";
        $form['double-tree']['operations']['relate_right']['#submit'] = array('term_relations_tm_double_tree_relate_submit');
        $form['double-tree']['operations']['relate_right']['#validate'] = array('term_relations_tm_double_tree_relate_validate');
        $form['double-tree']['operations']['relate_right']['#weight'] = 22;
        $form['double-tree']['operations']['relate_right']['#ajax']['callback'] = 'term_relations_tm_double_tree_ajax_callback';

        $form['double-tree']['operations']['show_relations_right']['#attributes']['title'] = t('Show relations right.');
        $form['double-tree']['operations']['show_relations_right']['#src'] = $module_path ."/images/show-relationships-right.png";
        $form['double-tree']['operations']['show_relations_right']['#submit'] = array('term_relations_tm_double_tree_show_relations_submit');
        $form['double-tree']['operations']['show_relations_right']['#validate'] = array('term_relations_tm_double_tree_show_relations_validate');
        $form['double-tree']['operations']['show_relations_right']['#weight'] = 23;
        $form['double-tree']['operations']['show_relations_right']['#ajax']['callback'] = 'term_relations_tm_double_tree_ajax_callback';
      }

      if ($supported_directionality == 'r2l' || $supported_directionality == 'bi') {
        // Relations buttons
        $form['double-tree']['operations']['relate_left'] = $form['double-tree']['operations']['move_left'];
        $form['double-tree']['operations']['show_relations_left'] = $form['double-tree']['operations']['move_left'];

        $form['double-tree']['operations']['relate_left']['#attributes']['title'] = t('Relate left.');
        $form['double-tree']['operations']['relate_left']['#src'] = $module_path ."/images/add-relationships-left.png";
        $form['double-tree']['operations']['relate_left']['#submit'] = array('term_relations_tm_double_tree_relate_submit');
        $form['double-tree']['operations']['relate_left']['#validate'] = array('term_relations_tm_double_tree_relate_validate');
        $form['double-tree']['operations']['relate_left']['#weight'] = 22;
        $form['double-tree']['operations']['relate_left']['#ajax']['callback'] = 'term_relations_tm_double_tree_ajax_callback';

        $form['double-tree']['operations']['show_relations_left']['#attributes']['title'] = t('Show relations left.');
        $form['double-tree']['operations']['show_relations_left']['#src'] = $module_path ."/images/show-relationships-left.png";
        $form['double-tree']['operations']['show_relations_left']['#submit'] = array('term_relations_tm_double_tree_show_relations_submit');
        $form['double-tree']['operations']['show_relations_left']['#validate'] = array('term_relations_tm_double_tree_show_relations_validate');
        $form['double-tree']['operations']['show_relations_left']['#weight'] = 23;
        $form['double-tree']['operations']['show_relations_left']['#ajax']['callback'] = 'term_relations_tm_double_tree_ajax_callback';
      }
    }

    // show relationship information
    $form['taxonomy']['manager']['tree']['#relations_vid'] = $vid2;
    $form['taxonomy2']['manager']['tree']['#relations_vid'] = $vid1;
  }
}

/**
 * Validation call back - only need to update 1 side
 */
function _term_relations_tm_double_tree_show_relations_validate($form, &$form_state) {
  $values = array();

  if (strpos($form_state['input']['_triggering_element_name'], 'right') !== FALSE) {
    //move right
    $values = array(
      'selected_terms' => $form_state['values']['taxonomy']['manager']['tree']['selected_terms'],
      'voc1' => $form_state['values']['voc']->vid,
      'voc2' => $form_state['values']['voc2']->vid,
      'expand_id' => 'right_tree_expand_terms',
      'update_tree_left' => FALSE,
      'update_tree_right' => TRUE,
      'direction' => 'right',
    );
  }
  else {
    //move left
    $values = array(
      'selected_terms' => $form_state['values']['taxonomy2']['manager']['tree']['selected_terms'],
      'voc1' => $form_state['values']['voc2']->vid,
      'voc2' => $form_state['values']['voc']->vid,
      'expand_id' => 'left_tree_expand_terms',
      'update_tree_left' => TRUE,
      'update_tree_right' => FALSE,
      'direction' => 'left',
    );
  }

  if (!count($values['selected_terms'])) {
    form_set_error('', t("No terms selected."));
  }
  form_set_value(array('#parents' => array('double_tree_values')), $values, $form_state);
}

/**
 * Submit handler for double tree form / show relations operation.
 */
function _term_relations_tm_double_tree_show_relations_submit($form, &$form_state) {
  $selected_tids = $form_state['values']['double_tree_values']['selected_terms'];
  $voc1 = $form_state['values']['double_tree_values']['voc1'];
  $voc2 = $form_state['values']['double_tree_values']['voc2'];
  $expand_id = $form_state['values']['double_tree_values']['expand_id'];

  $voc1 = taxonomy_vocabulary_load($voc1);
  $voc2 = taxonomy_vocabulary_load($voc2);

  $related_directions = term_relations_get_related_directions($selected_tids, 'tid', $voc2->vid);
  $all_related_tids = array();
  foreach($related_directions as $tid => $relations_by_direction) {
    $all_related_tids = array_merge($all_related_tids, $relations_by_direction['relations']);
  }
  $all_related_tids = array_unique($all_related_tids);


  $related_lrbi = _term_relations_tm_convert_to_lrbi($related_directions);

  $form_state['values']['double_tree_values'][$expand_id] = $all_related_tids;
  $form_state['values']['double_tree_values']['relations'] = $related_lrbi;

  $form_state['rebuild'] = TRUE;
}

/**
 * Validation call back - only need to update 1 side
 */
function _term_relations_tm_double_tree_relate_validate($form, &$form_state) {
  $values = array();

  if (strpos($form_state['input']['_triggering_element_name'], 'right') !== FALSE) {
    //move right
    $values = array(
      'selected_from_terms' => $form_state['values']['taxonomy']['manager']['tree']['selected_terms'],
      'selected_to_terms' => $form_state['values']['taxonomy2']['manager']['tree']['selected_terms'],
      // 'right_tree_expand_terms' => $form_state['values']['taxonomy']['manager']['tree']['selected_terms'],
      'voc1' => $form_state['values']['voc']->vid,
      'voc2' => $form_state['values']['voc2']->vid,
      'expand_id' => 'right_tree_expand_terms',
      'update_tree_left' => FALSE,
      'update_tree_right' => FALSE,
      'direction' => 'right',
    );
  }
  else {
    //move left
    $values = array(
      'selected_to_terms' => $form_state['values']['taxonomy']['manager']['tree']['selected_terms'],
      'selected_from_terms' => $form_state['values']['taxonomy2']['manager']['tree']['selected_terms'],
      'voc1' => $form_state['values']['voc2']->vid,
      'voc2' => $form_state['values']['voc']->vid,
      'expand_id' => 'left_tree_expand_terms',
      'update_tree_left' => FALSE,
      'update_tree_right' => FALSE,
      'direction' => 'left',
    );
  }

  if (!count($values['selected_from_terms'])) {
    form_set_error('', t("No terms selected to relate."));
  }
  if (!count($values['selected_to_terms'])) {
    form_set_error('', t("No terms selected to relate to."));
  }

  form_set_value(array('#parents' => array('double_tree_values')), $values, $form_state);
}

/**
 * Submit handler for double tree form / show relations operation.
 */
function _term_relations_tm_double_tree_relate_submit($form, &$form_state) {
  $selected_relate_from_terms = $form_state['values']['double_tree_values']['selected_from_terms'];
  $selected_relate_to_terms = $form_state['values']['double_tree_values']['selected_to_terms'];
  $voc1 = $form_state['values']['double_tree_values']['voc1'];
  $voc2 = $form_state['values']['double_tree_values']['voc2'];
  $expand_id = $form_state['values']['double_tree_values']['expand_id'];
  $direction = $form_state['values']['double_tree_values']['direction'];

  $voc1 = taxonomy_vocabulary_load($voc1);
  $voc2 = taxonomy_vocabulary_load($voc2);


  // put a guard in to protect against spoofed requests
  $supported_directionality = _term_relations_tm_get_supported_directions($voc1->vid, $voc2->vid);

  if ($supported_directionality == 'bi' || $supported_directionality == 'l2r') {
    term_relations_tm_relate($selected_relate_from_terms, $voc1->vid, $selected_relate_to_terms, $voc2->vid);

    // display the relationships
    $related_directions = term_relations_get_related_directions($selected_relate_from_terms, 'tid', $voc2->vid);
    $related_lrbi = _term_relations_tm_convert_to_lrbi($related_directions);

    // $form_state['values']['double_tree_values'][$expand_id] = $relations['all_related_tids'];
    $form_state['values']['double_tree_values']['relations'] = $related_lrbi;

    // need to update affected terms relations indicators
    foreach($selected_relate_from_terms as $tid) {
      if(!empty($related_directions[$tid])) {
        $direction_indicator = _term_relations_tm_get_relation_indicator($related_directions[$tid]);
        $form_state['values']['double_tree_values']['relation_direction']['left'][$tid] = $direction_indicator;

        $classes = _term_relations_tm_get_relation_indicator_classes($direction_indicator);
        $form_state['values']['double_tree_values']['relation_classes']['left'][$tid] = implode(' ', $classes);
      }
      else {
        $form_state['values']['double_tree_values']['relation_classes']['left'][$tid] = '';
        $form_state['values']['double_tree_values']['relation_direction']['left'][$tid] = 'none';
      }
    }

    // need update classes for RH terms
    $related_directions = term_relations_get_related_directions($selected_relate_to_terms, 'tid', $voc->vid);
    foreach($selected_relate_to_terms as $tid) {
      if(!empty($related_directions[$tid])) {
        $direction_indicator = _term_relations_tm_get_relation_indicator($related_directions[$tid]);
        $form_state['values']['double_tree_values']['relation_direction']['right'][$tid] = $direction_indicator;

        $classes = _term_relations_tm_get_relation_indicator_classes($direction_indicator);
        $form_state['values']['double_tree_values']['relation_classes']['right'][$tid] = implode(' ', $classes);
      }
      else {
        $form_state['values']['double_tree_values']['relation_classes']['right'][$tid] = '';
        $form_state['values']['double_tree_values']['relation_direction']['right'][$tid] = 'none';
      }
    }
    // don't rebuild the forms - we'll just update the affected terms via ajax.
  }
}

/**
 * Override of double tree AJAX callback. Add relationship settings to the
 * response which can be picked up by behaviours attach method to update
 * trees.
 */
function _term_relations_tm_double_tree_ajax_callback($form, $form_state) {
  $ajax = taxonomy_manager_double_tree_ajax_callback($form, $form_state);
  if(!empty($form_state['values']['double_tree_values']['relations']) || !empty($form_state['values']['double_tree_values']['relation_classes'])) {
    $setting['termRelationsTM']['voc1'] = $form_state['values']['double_tree_values']['voc1'];
    $setting['termRelationsTM']['voc2'] = $form_state['values']['double_tree_values']['voc2'];
    $setting['termRelationsTM']['direction'] = $form_state['values']['double_tree_values']['direction'];
    if(!empty($form_state['values']['double_tree_values']['relation_classes'])) {
      $setting['termRelationsTM']['relationClasses'] = $form_state['values']['double_tree_values']['relation_classes'];
    }
    if(!empty($form_state['values']['double_tree_values']['relation_direction'])) {
      $setting['termRelationsTM']['relationDirection'] = $form_state['values']['double_tree_values']['relation_direction'];
    }
    if(!empty($form_state['values']['double_tree_values']['relations'])) {
      $setting['termRelationsTM']['relations'] = $form_state['values']['double_tree_values']['relations'];
    }

    $ajax['#commands'][] = ajax_command_settings($setting, FALSE);
  }

  return $ajax;
}

/**
 * Override of taxonomy_manager_tree_build_child_form callback for generating and rendering nested child forms (AHAH).
 * I hate to duplicate so much code but without patching it's the only way.
 *
 * @todo Investigate better extension points. Potentially behaviours and form_alter but not at the mo.
 *
 * @param $tree_id
 * @param $parent term id of parent, that is expanded and of which children have to be loaded
 * @param $tid Term to expand, may be a single tid or an array of tids.
 */
function term_relations_tm_tree_build_child_form($tree_id, $vid, $parent, $tid = 0) {
  $params = $_GET;
  $form_state = form_state_defaults();
  $form_state['method'] = 'get';
  $form_state['values'] = array();
  $form_state['process_input'] = TRUE;
  $form_state['input'] = array();
  $form_state['complete form'] = array();

  // @expand_terms
  // this for multiple term expansion but now duplicated with patch http://drupal.org/node/980556
  $tids = array();
  if (isset($tid) && $tid != 0) {
    // convert to array
    $tids = explode(',', $tid);
  }

  if (count($tids) == 1) {
    $language = _taxonomy_manager_term_get_lang($tids[0]);
  }
  else {
    $language = $params['language'];
  }

  // First #process callback adds multiple term expand - better as patch.
  // Second callback adds relationship info without having to override the tree
  // form builder as well - taxonomy_manager_tree_build_form().
  $child_form = array(
    '#type' => 'taxonomy_manager_tree',
    '#vid' => $vid,
    '#parent' => $parent,
    '#pager' => TRUE,
    '#language' => $language,
    '#term_to_expand' => $tids, // handle multiple tids
    '#siblings_page' => 1,
    '#process' => array(
      // @expand_terms
      // @todo - if patch http://drupal.org/node/980556 is committed revert to standard process
      // function taxonomy_manager_tree_process_elements.
      'term_relations_tm_tree_process_elements',
      'term_relations_tm_tree_process_elements_add_relationships',
    ),
  );


  if (!variable_get('term_relations_tm_disable_relationships_extra', 0)) {
    // set relations vid for displaying a has relationships flag
    if ($params['relations_vid']) {
      $child_form['#relations_vid'] = $params['relations_vid'];
    }
  }

  $opertions_callback = 'taxonomy_manager_'. str_replace('-', '_', $tree_id) .'_operations';
  if (function_exists($opertions_callback)) {
    $child_form['#operations_callback'] = $opertions_callback;
  }

  $link_callback = 'taxonomy_manager_'. str_replace('-', '_', $tree_id) .'_link';
  if (function_exists($link_callback)) {
    $child_form['#link_callback'] = $link_callback;
  }

  _taxonomy_manager_tree_sub_forms_set_parents($tree_id, $parent, $child_form);

  $child_form = form_builder('taxonomy_manager_form', $child_form, $form_state);

  print drupal_json_output(array('data' => drupal_render($child_form)));
  ajax_footer();
}

/**
 * Callback to do extra processing to the tree.
 */
function _term_relations_tm_tree_process_elements_add_relationships($element) {
  global $_term_relations_tm_existing_ids; //TEMP: seems like this functions gets called twice in preview and cause problem because of adding the settings to js twice

  // Add relations
  if ($element['#relations_vid']) {

    $_term_relations_tm_existing_ids = is_array($_term_relations_tm_existing_ids) ? $_term_relations_tm_existing_ids : array();

    $module_path = drupal_get_path('module', 'term_relations_tm');
    $id = drupal_clean_css_identifier(implode('-', $element['#parents']));

    if (!$element['#siblings_page'] && !in_array($id, $_term_relations_tm_existing_ids)) {
      if(empty($_term_relations_tm_existing_ids)) {
        drupal_add_css($module_path .'/css/term_relations_tm.css');
        drupal_add_js($module_path .'/js/term_relations_tm.js', array('weight' => 3));

        drupal_add_js(array('termRelations' => array('url' => url('admin/structure/taxonomy_manager/siblingsform'), 'modulePath' => $module_path)), 'setting');
      }

      $_term_relations_tm_existing_ids[$id] = $id;
    }

    // Add relationships information - used to add operation and class.
    _term_relations_tm_tree_add_relationships_elements($element['#elements'], $element['#relations_vid'], $element['#term_to_expand']);
  }

  return $element;
}

/**
 * Add relationship info to an array of elements and recurse into their children.
 */
function _term_relations_tm_tree_add_relationships_elements(&$elements, $related_vid, $terms_to_expand) {
  $relationships = term_relations_get_related_directions(array_keys($elements), 'tid', $related_vid);
  foreach ($elements as $tid => &$element) {
    if (!empty($element['children'])) {
      // recursive call for children
      _term_relations_tm_tree_add_relationships_elements($element['children'], $related_vid, $terms_to_expand);
    }

    if (!empty($relationships[$tid])) {
      $direction_indicator = _term_relations_tm_get_relation_indicator($relationships[$tid]);
      $classes = _term_relations_tm_get_relation_indicator_classes($direction_indicator);
      $element['#attributes']['class'] = !empty($element['#attributes']['class']) ? array_merge($element['#attributes']['class'], $classes) : $classes;
    }

    if(!empty($terms_to_expand[$tid])) {
      $element['#attributes']['class'][] = 'show-related';
    }
  }
}

/**
 * 'Private' implementation of hook_form_FORM_ID_alter() for form id 'taxonomy_manager_form'.
 * Add relationships selector to Taxonomy Manager term edit form.
 * When to do this - only when core relationships enabled?
 */
function _term_relations_tm_form_taxonomy_manager_form_alter(&$form, &$form_state) {
  $vocab2 = $form_state['build_info']['args'][1];
  if(!empty($form['term_data']['#term'])) {
    $term = taxonomy_term_load($form['term_data']['#term']['tid']);
    $relations = term_relations_get_related($term->tid);
    $related_terms = taxonomy_term_load_multiple($relations);
    $form['term_data']['relations'] = _term_relations_tm_form_term_data_relations($term, $related_terms, t('Relations'), 'relation', TRUE, TRUE, TRUE);
    $form['term_data']['relations']['#weight'] = '53';
  }

}

function _term_relations_tm_form_term_data_relations($term, $values, $header_type, $attr, $autocomplete = TRUE, $add = TRUE, $ajax = FALSE) {
  $form = _taxonomy_manager_form_term_data_parents($term, $values, $header_type, $attr, $autocomplete, $add, $ajax);
  foreach($form['data'] as $id => $value_element) {
    $form['data'][$id]['remove']['#submit'] = array('term_relations_tm_term_data_form_submit_relations_remove');
    $form['data'][$id]['remove']['#validate'] = array('term_relations_tm_term_data_form_validate_relations_remove');
    if ($ajax) {
      $form['data'][$id]['remove']['#ajax']['callback'] = 'term_relations_tm_term_data_form_ajax_callback';
    }
  }

  if($add) {
    $form['op']['add_button']['#submit'] = array('term_relations_tm_term_data_form_submit_relations_add');
    $form['op']['add_button']['#validate'] = array('term_relations_tm_term_data_form_validate_relations_add');
    if ($ajax) {
      $form['op']['add_button']['#ajax']['callback'] = 'term_relations_tm_term_data_form_ajax_callback';
    }
  }
  return $form;
}

/**
 * @see taxonomy_manager_term_data_form_validate_parents_remove in taxonomy_manager.module
 */
function _term_relations_tm_term_data_form_validate_relations_remove($form, &$form_state) {
  $splits = explode('][', $form_state['triggering_element']['#name']);
  if (count($splits) == 3 && is_numeric($splits[1])) {
    form_set_value(array('#parents' => array('remove_relation_tid')), $splits[1], $form_state);
  }
  else {
    form_set_error('relations', t('Invalid term selection.'));
  }
}

function term_relations_tm_term_data_form_submit_relations_remove($form, &$form_state) {
  $tid = $form_state['values']['tid'];
  $relations = term_relations_get_related($tid);

  $related_tid_to_remove = $form_state['values']['remove_relation_tid'];
  unset($relations[$related_tid_to_remove]);

  // remove the relation
  term_relations_set_related($tid, $relations);

  drupal_set_message(t("Removed relationship"));
  $form_state['rebuild'] = TRUE;
  form_set_value(array('#parents' => array('tree-update')), array('update' => TRUE), $form_state);
}

/**
 * @see taxonomy_manager_term_data_form_validate_relations_add in taxonomy_manager.module
 */
function _term_relations_tm_term_data_form_validate_relations_add($form, &$form_state) {
  $term_data = $form_state['values'];
  $term = taxonomy_term_load($term_data['tid']);

  $relation_names = $term_data['relations']['op']['add'];
  $typed_terms = taxonomy_manager_autocomplete_tags_get_tids($relation_names, $term->vid, FALSE);

  $error_msg = "";

  if ($relation_names == "") {
    form_set_error('relations][op][add', t("Relations field is required."));
  }
  else if (_taxonomy_manager_check_duplicates($term->vid, $relation_names, $error_msg)) {
    form_set_error('relations][op][add', t("Warning: Your input matches with multiple terms, because of duplicated term names. Please enter a unique term name or the term id with 'term-id:[tid]'" ." (!error_msg).", array('!error_msg' => $error_msg)));
  }
  else if(!taxonomy_manager_check_language($term->vid, array($term->tid), $typed_terms)) {
    form_set_error('relations][op][add', t("Terms must be of the same language."));
  }
}

function term_relations_tm_term_data_form_submit_relations_add($form, &$form_state) {
  $term_data = $form_state['values'];
  $term = taxonomy_term_load($term_data['tid']);

  $relation_names = $term_data['relations']['op']['add'];

  $typed_terms = taxonomy_manager_autocomplete_tags_get_tids($relation_names, $term->vid, TRUE); //insert terms

  $related_tids = array_keys(term_relations_get_related($term->tid, 'tid'));
  $new_related_tids = array_keys($typed_terms);
  $updated_related_tids = array_merge($related_tids, $new_related_tids);
  term_relations_set_related($term->tid, $updated_related_tids);

  drupal_set_message(t('Updated relations'));
  $form_state['rebuild'] = TRUE;
  form_set_value(array('#parents' => array('tree-update')), array('update' => TRUE), $form_state);
}



/**
 * Override of term data form AJAX callback.
 *
 * @todo Add relationship settings to the response which can be picked up by
 * behaviours attach method to update trees.
 */
function _term_relations_tm_term_data_form_ajax_callback($form, $form_state) {
  $ajax = taxonomy_manager_term_data_form_ajax_callback($form, $form_state);

  return $ajax;
}


