(function ($) {

Drupal.behaviors.termRelations = {
  /**
   * Attach term relation behaviours. If settings.termRelationsTM is set then
   * re-attach behaviours after ajax request.
   */
  attach: function(context, settings) {
    if(settings.termRelationsTM && $(context).is('#taxonomy-manager-double-tree-form')) {
      Drupal.settings.termRelationsTM = settings.termRelationsTM;

      var tree1 = Drupal.settings.taxonomytree.tree[settings.termRelationsTM.direction == 'right' ? 0 : 1];
      var tree2 = Drupal.settings.taxonomytree.tree[settings.termRelationsTM.direction == 'right' ? 1 : 0];

      $(".highlightActiveTerm", tree1.ul).removeClass('highlightActiveTerm');
      $(".highlightActiveTerm", tree2.ul).removeClass('highlightActiveTerm');

      if(settings.termRelationsTM.relations) {
        for(tid in settings.termRelationsTM.relations) {
          // highlight term
          tree1.highlightTerm(tid);

          // for each direction
          for(direction in settings.termRelationsTM.relations[tid]) {
            // for each related tid
            for(rtid in settings.termRelationsTM.relations[tid][direction]) {
              // highlight related term in other tree
              tree2.highlightTerm(rtid);
            }
          }
        }
      }

      if(settings.termRelationsTM.relationClasses) {
        var treeId;
        for(treeId in settings.termRelationsTM.relationClasses) {
          var tree = treeId == 'left' ? tree1 : tree2;
          var tid;
          for(tid in settings.termRelationsTM.relationClasses[treeId]) {
            tree.setRelationClasses(tid, settings.termRelationsTM.relationClasses[treeId][tid]);
          }
        }

        for(treeId in settings.termRelationsTM.relationDirection) {
          var tree = treeId == 'left' ? tree1 : tree2;
          var tid;
          for(tid in settings.termRelationsTM.relationDirection[treeId]) {
            tree.setRelationFlag(tid, settings.termRelationsTM.relationDirection[treeId][tid]);
          }
        }
      }
    }
    else {
      // attach relationships to initial trees
      if(Drupal.settings.taxonomytree.tree.length == 2) {
        for(var i = 0; i < Drupal.settings.taxonomytree.tree.length; i++) {
          var tree = Drupal.settings.taxonomytree.tree[i];
          $(tree.ul).once('term-relations-tm', function() {
            tree.attachRelationships(0);
          });
        }
      }
    }
  }
}

/**
 * Set the relation classes on a terms list element.
 */
Drupal.TaxonomyManagerTree.prototype.setRelationClasses = function(tid, classes) {
  var termLi = this.getLi(tid);
  if(termLi != "") {
    $(termLi).removeClass('has-related l2r r2l bi').addClass(classes);
  }
}

/**
 * Sets the relationship flag on a term. Appends new element if doesn't exist, or
 * removes it if it does exist and direction is none.
 */
Drupal.TaxonomyManagerTree.prototype.setRelationFlag = function(tid, direction) {
  var direction = direction;
  var termLi = this.getLi(tid);

  var label = $('> .term-line > .form-item > label', termLi);
  $(label).not(':has(.has-related-flag)').each(function() {
    var directionIndicator = Drupal.theme('termRelationsTMFlag', direction);
    $(this).append('<span class="has-related-flag">r'+directionIndicator+'</span>');
  })
  .end()
  .find('.has-related-flag').each(function() {
    if(direction == 'none') {
      $(this).remove();
    }
    else {
      var directionIndicator = Drupal.theme('termRelationsTMFlag', direction);
      $(this).text(directionIndicator);
    }
  })
}

// Given tid find term link element in tree list and highlight it
Drupal.TaxonomyManagerTree.prototype.highlightTerm = function(tid) {
  var termLink = $(".term-id:input[value="+ tid +"]", this.ul).parent().find("a.term-data-link");
  if(termLink != "") {
    $(termLink).parent().addClass('highlightActiveTerm')
  }
}

/**
 * Theme function to return a directed or undirected relationship flag.
 */
Drupal.theme.prototype.termRelationsTMFlag = function (direction) {
  if(direction == 'bi') {
    return 'r <>';
  }
  else if(direction == 'l2r') {
    return 'r ->';
  }
  else if(direction == 'r2l') {
    return 'r <-';
  }
  else if(direction == 'na') {
    return 'r';
  }
};

/**
 * Adds relationship flags with click event to term labels in the tree structure.
 * The click event reveals and highlights related terms in the opposite tree.
 */
Drupal.TaxonomyManagerTree.prototype.attachRelationships = function(otherTree) {
  var otherTree = otherTree;
  $('li.has-related .has-related-flag', this.ul).remove();
  $('li.has-related:not(.l2r,.r2l,.bi) > .term-line > .form-item > label', this.ul).append('<span class="has-related-flag">'+Drupal.theme('termRelationsTMFlag', 'na')+'</span>');
  $('li.has-related.l2r > .term-line > .form-item > label', this.ul).append('<span class="has-related-flag">'+Drupal.theme('termRelationsTMFlag', 'l2r')+'</span>');
  $('li.has-related.r2l > .term-line > .form-item > label', this.ul).append('<span class="has-related-flag">'+Drupal.theme('termRelationsTMFlag', 'r2l')+'</span>');
  $('li.has-related.bi > .term-line > .form-item > label', this.ul).append('<span class="has-related-flag">'+Drupal.theme('termRelationsTMFlag', 'bi')+'</span>');
  // @todo reinstate flag click.
  // $('li.has-related > .term-line label .has-related-flag', ul).click(function() {
  //   var li = $(this).parents("li:first");
  //   var tid = Drupal.getTermId(li);
  //   // get related - no reliable way to get the related tids - would put a map in settings but would need to change child form gets returns etc.
  //   var params = {};
  //   params['selected_terms['+ tid +']'] = tid;
  //   params['voc2'] = otherTree.vocId;
  //   params['op'] = 'show_relations';
  //
  //   $.ajax({
  //     data: params,
  //     type: "POST",
  //     url: Drupal.settings.DoubleTree['url'],
  //     dataType: 'json',
  //     success: function(response, status) {
  //       var allRelatedTids = [];
  //
  //       for (var i in response.related_tids) {
  //         allRelatedTids = allRelatedTids.concat(response.related_tids[i]);
  //       }
  //
  //       otherTree.loadRootForm(allRelatedTids);
  //       $("#"+ tree.treeId).find(".highlightActiveTerm").removeClass('highlightActiveTerm');
  //       Drupal.activeTermSwapHighlight($('> .term-line a.term-data-link', li));
  //     }
  //   });
  //
  //   return false;
  // });
}

/*****************************************************************************
 * TaxonomyManagerTree prototype overrides.
 * This is the easiest way of getting extra params into AJAX callbacks, and
 * doing extra processing on success.
 * @todo - look for possible extension points and propose patch to TM.
 ****************************************************************************/

/**
 * Another fudge to make the tree objects globally available.
 */
Drupal.TaxonomyManagerTree.prototype.attachTreeview = function(ul, currentIndex) {
  // ************ I think we need a patch ************************************
  if (Drupal.settings.taxonomytree.id.length == 2) {
    if(!Drupal.settings.taxonomytree.tree) {
      Drupal.settings.taxonomytree.tree = [];
    }
    var i = 0;
    for (i = 0; i < Drupal.settings.taxonomytree.id.length; i++) {
      if(Drupal.settings.taxonomytree.id[i] == this.treeId) {
        Drupal.settings.taxonomytree.tree[i] = this;
        break;
      }
    }
  }
  // ************ End of I think we need a patch *****************************

 var tree = this;
 if (currentIndex) {
   ul = $(ul).slice(currentIndex);
 }
 var expandableParent = $(ul).find("div.hitArea");
 $(expandableParent).click(function() {
   var li = $(this).parent();
   tree.loadChildForm(li);
   tree.toggleTree(li);
 });
 $(expandableParent).parent("li.expandable, li.lastExpandable").children("ul").hide();
}

/**
 * Our own version of loadChildForm....
 * loads child terms and appends html to list
 * adds treeview, weighting etc. js to inserted child list
 */
Drupal.TaxonomyManagerTree.prototype.loadChildForm = function(li, update, callback) {
 var tree = this;
 if ($(li).is(".has-children") || update == true) {
   $(li).removeClass("has-children");
   if (update) {
     $(li).children("ul").remove();
   }
   var parentId = Drupal.getTermId(li);
   if (!(Drupal.settings.childForm['url'] instanceof Array)) {
     url = Drupal.settings.childForm['url'];
   }
   else {
     url = Drupal.settings.childForm['url'][0];
   }
   url += '/'+ this.treeId +'/'+ this.vocId +'/'+ parentId;
   var param = new Object();
   param['form_build_id'] = this.form_build_id;
   param['form_id'] = this.form_id;
   param['tree_id'] = this.treeId;
   param['language'] = this.language;

   // ******* Start of additional code *******
   // if in double tree context attach relationships
   var otherTree;
   if (Drupal.settings.taxonomytree.id.length == 2) {
     var i = 0;
     for (i = 0; i < Drupal.settings.taxonomytree.id.length; i++) {
       if(Drupal.settings.taxonomytree.id[i] != this.treeId) {
         param['relations_vid'] = Drupal.settings.taxonomytree.vid[i];
         otherTree = Drupal.settings.taxonomytree.tree[i];
         break;
       }
     }
   }
   // ******* End of additional code *********


   $.ajax({
     data: param,
     type: "GET",
     url: url,
     dataType: 'json',
     success: function(response, status) {
        $(li).append(response.data);
       var ul = $(li).children("ul");
       tree.attachTreeview(ul);
       tree.attachSiblingsForm(ul);
       tree.attachSelectAllChildren(ul);

       //only attach other features if enabled!
       var weight_settings = Drupal.settings.updateWeight || [];
       if (weight_settings['up']) {
         Drupal.attachUpdateWeightTerms(li);
       }
       var term_data_settings = Drupal.settings.termData || [];
       if (term_data_settings['url']) {
         Drupal.attachTermDataLinks(ul);
       }
       // ******* Start of additional code *******
       if(otherTree) {
         tree.attachRelationships(otherTree);
       }
       // ******* End of additional code *********
       if (typeof(callback) == "function") {
         callback(li, tree);
       }
     }
   });
 }
}

})(jQuery);
