<?php

/**
 * @file
 * Code that provides additional tree functionality. Not specific to term relationships.
 * Specifically it allows multiple child terms to be expanded in one Ajax call.
 * @todo Would be much better as a patch - http://drupal.org/node/980556 - in which
 * case this file can disappear.
 */

/**
 * Processes the tree form element.
 *
 * Very similar to original but adds multiple term expansion.
 *
 * @param $element
 * @return the tree element
 */
function _term_relations_tm_tree_process_elements($element) {
  global $_taxonomy_manager_existing_ids; //TEMP: seems like this functions gets called twice in preview and cause problem because of adding the settings to js twice
  $_taxonomy_manager_existing_ids = is_array($_taxonomy_manager_existing_ids) ? $_taxonomy_manager_existing_ids : array();

  $module_path = drupal_get_path('module', 'taxonomy_manager') .'/';
  $id = drupal_clean_css_identifier(implode('-', $element['#parents']));
  $element['#id'] = $id;
  $vid = $element['#vid'];

  if (!$element['#siblings_page'] && !in_array($id, $_taxonomy_manager_existing_ids)) {
    if(empty($_taxonomy_manager_existing_ids)) {
      drupal_add_css($module_path .'css/taxonomy_manager.css');
      drupal_add_js($module_path .'js/tree.js');

      drupal_add_js(array('siblingsForm' => array('url' => url('admin/structure/taxonomy_manager/siblingsform'), 'modulePath' => $module_path)), 'setting');
      drupal_add_js(array('childForm' => array('url' => url('admin/structure/taxonomy_manager/childform'), 'modulePath' => $module_path)), 'setting');
    }

    $_taxonomy_manager_existing_ids[$id] = $id;

    drupal_add_js(array('taxonomytree' => array('id' => $id, 'vid' => $vid)), 'setting');
  }

  if (empty($element['#operations'])) {
    $opertions_callback = 'taxonomy_manager_'. implode('_', $element['#parents']) .'_operations';
    if (function_exists($opertions_callback)) {
      $element['#operations_callback'] = $opertions_callback;
    }
  }
  if (!isset($element['#link'])) {
    $link_callback = 'taxonomy_manager_'. implode('_', $element['#parents']) .'_link';
    if (function_exists($link_callback)) {
      $element['#link_callback'] = $link_callback;
    }
  }

  $element['#elements'] = array();
  $tree = _taxonomy_manager_tree_get_item($element['#vid'], $element['#parent'], $element['#pager'], $element['#siblings_page'], $element['#search_string'], $element['#language']);
  if (count($tree)) {
    if ($element['#pager'] && !($element['#parent'] || $element['#siblings_page'])) {
      $element['pager'] = array('#value' => theme('pager'));
    }

    $terms_to_expand = array();
    if (isset($element['#term_to_expand'])) {
      // **************** Start of replacement code **********************
      // allow multiple terms to be expanded
      $requested_terms_to_expand = is_array($element['#term_to_expand']) ? $element['#term_to_expand'] : array($element['#term_to_expand']);
      $tree = _term_relations_tm_tree_get_all_paths($requested_terms_to_expand, $tree);
      $terms_to_expand = taxonomy_manager_tree_get_terms_to_expand($tree, $requested_terms_to_expand, TRUE);
      // **************** End of replacement code ************************
    }

    if (count($element['#default_value']) && !$element['#expand_all']) {
      $terms_to_expand = taxonomy_manager_tree_get_terms_to_expand($tree, $element['#default_value'], $element['#multiple']);
    }

    if (!empty($element['#language'])) {
      $element['#elements']['language'] = array('#type' => 'hidden', '#value' => $element['#language'], '#attributes' => array('class' => 'tree-lang'));
      _taxonomy_manager_tree_element_set_params($element['#parents'], $element['#elements']);
    }

    $index = 0;
    taxonomy_manager_tree_build_form($index, $tree, $element['#elements'], $element, $element['#parents'], $element['#siblings_page'], $element['#default_value'], $element['#multiple'], $terms_to_expand);
  }

  return $element;
}

/**
 *
 * This version supports multiple term expansion.
 * This function call by element process function _term_relations_tm_tree_process_elements.
 */
function _term_relations_tm_tree_get_all_paths($tids, $tree, $first_parent = TRUE) {
  $path = array();

  // Get all tids that need to be included in tree - i.e. $tids and their parents.
  $tids_in_paths = array();
  foreach($tids as $tid) {
    _term_relations_tm_tree_recurse_parents($tid, $tids_in_paths, $first_parent);
  }

  $new_tree = array();

  foreach($tree as $index => $leaf) {
    $new_tree[] = $leaf;
    $sub_tree = _term_relations_tm_tree_recurse_children($leaf->tid, $tids_in_paths, 1);

    if(!empty($sub_tree)) {
      $new_tree = array_merge($new_tree, $sub_tree);
    }
  }

  return $new_tree;
}

/**
 * Build up list of tids on all paths to tid by recursing it's parents. If $first_parent is set
 * and terms in path have multiple parents then only the first parent is included in
 * the path.
 */
function _term_relations_tm_tree_recurse_parents($tid, &$tids_in_paths, $first_parent = TRUE) {
  if(!isset($tids_in_paths[$tid])) {
    $tids_in_paths[$tid] = $tid;
    if($parents = taxonomy_get_parents($tid)) {

      foreach($parents as $parent) {
        _term_relations_tm_tree_recurse_parents($parent->tid, $tids_in_paths);
        if($first_parent) {
          // only include path to first parent
          break;
        }
      }
    }
  }
}

/**
 * Add all children of a term to tree if any of them are included in term paths.
 * For children that are included in term path - recursively call.
 * @todo guard against circular reference.
 */
function _term_relations_tm_tree_recurse_children($tid, $tids_in_paths, $depth = 0) {
  $children = taxonomy_get_children($tid);
  $children_in_path = array_intersect(array_keys($children), $tids_in_paths);

  $tree = array();

  if(!empty($children_in_path)) {
    // include all these children and recurse into the children in the path
    foreach($children as $child) {
      // clone child - if has multiple parents then ->parents property will be different for each.
      $this_child = clone $child;
      $this_child->parents = array($tid);
      $this_child->depth = $depth;
      $tree[] = $this_child;
      if(in_array($this_child->tid, $children_in_path)) {
        $tree = array_merge($tree, _term_relations_tm_tree_recurse_children($this_child->tid, $tids_in_paths, $depth + 1));
      }
    }
  }
  return $tree;
}
