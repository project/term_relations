<?php

/**
 * @file
 * Supporting functions here to save overhead of parsing infrequently used functions.
 */

require_once 'term_relations_tm.tree.inc';

/**
 * Determine which relationships may be created between two vocabularies.
 */
function _term_relations_tm_get_supported_directions($vid1, $vid2) {
  // If the term_relations_vocab module has not been enabled then can only relate terms in the same vocabulary.
  if (!module_exists('term_relations_vocab')) {
    if ($vid1 == $vid2) {
      if (term_relations_is_directed()) {
        // both directions allowed.
        return 'bi';
      }
      else {
        // only one direction allowed - doesn't matter which.
        return 'l2r';
      }
    }
  }
  else {
    // get mapping of allowed relationships
    $all_vocab_relationships = term_relations_vocab_get_relationships();
    if (term_relations_is_directed()) {
      if (!empty($all_vocab_relationships[$vid1]['l2r'][$vid2]) && !empty($all_vocab_relationships[$vid1]['r2l'][$vid2])) {
        return 'bi';
      }
      elseif (!empty($all_vocab_relationships[$vid1]['l2r'][$vid2])) {
        return 'l2r';
      }
      elseif (!empty($all_vocab_relationships[$vid1]['r2l'][$vid2])) {
        return 'r2l';
      }
    }
    else {
      // only one direction - preserve direction - but is there any point?
      if (!empty($all_vocab_relationships[$vid1]['l2r'][$vid2])) {
        return 'l2r';
      }
      elseif (!empty($all_vocab_relationships[$vid1]['r2l'][$vid2])) {
        return 'r2l';
      }
    }
  }

  return FALSE;
}

/**
 * 'Private' implmentation of hook_taxonomy_manager_term_data_submit().
 * @todo - implement this!
 */
function _term_relations_tm_taxonomy_manager_term_data_submit($param, $values) {

}

/**
 * Get classes used to flag a term as related.
 */
function _term_relations_tm_get_relation_indicator_classes($direction) {
  $classes = array('has-related');
  if($direction && $direction != 'na') {
    $classes[] = $direction;
  }
  return $classes;
}

/**
 * Get the relation indicator - a summary of all the terms relationships.
 */
function _term_relations_tm_get_relation_indicator($term_directions) {
  if (term_relations_is_directed()) {
    if (!empty($term_directions['related_to']) && !empty($term_directions['related_by'])) {
      return 'bi';
    }
    else if(!empty($term_directions['related_to'])) {
      return 'l2r';
    }
    else if(!empty($term_directions['related_by'])) {
      return 'r2l';
    }
  }
  else {
    // not applicable
    return 'na';
  }
}

/**
 * Convert related_to, related_by data structure to mutually exclusive lists
 * of bi-directional (bi), left-to-right (l2r), and right-to-left (r2l) relationships.
 *
 * @param $related_directions
 *  Array with elements:
 *  - 'related_to' => Array of left-to-right related term tids.
 *  - 'related_by' => Array of right-to-left related term tids.
 *  - 'relations' => Array of all related term tids.
 *
 * @return
 *  Array with elements:
 *  - 'bi' => Array of bi-directionally related term tids.
 *  - 'l2r' => Array of left-to-right related term tids.
 *  - 'r2l' => Array of right-to-left related term tids.
 */
function _term_relations_tm_convert_to_lrbi($related_directions) {
  $related_lrbi = array();
  foreach($related_directions as $tid => $term_directions) {

    if(!empty($term_directions['related_to']) && !empty($term_directions['related_by'])) {
      $bi = array_intersect($term_directions['related_to'], $term_directions['related_by']);
      $l2r = array_diff($term_directions['related_to'], $term_directions['related_by']);
      $r2l = array_diff($term_directions['related_by'], $term_directions['related_to']);
      $related_lrbi[$tid] = array('l2r' => $l2r, 'r2l' => $r2l, 'bi' => $bi);
    }
    elseif (!empty($term_directions['related_to'])) {
      $related_lrbi[$tid] = array('l2r' => $term_directions['related_to']);
    }
    elseif (!empty($term_directions['related_by'])) {
      $related_lrbi[$tid] = array('r2l' => $term_directions['related_by']);
    }
  }
  return $related_lrbi;
}

/**
 * Relates terms from one vocabulary to terms in the same or other vocabulary. If a relationship already exists it is deleted.
 *
 * @param array $relate_from_tids
 *  Array of LHS term tids to add relationships to.
 * @param $vid_from
 *  The id of the vocabulary that the LHS terms belong to.
 * @param array $relate_to_terms
 *  Array of RHS term tids to relate to.
 * @param $vid_to
 *  The id of the vocabulary that the RHS terms belong to.
 */
function term_relations_tm_relate($relate_from_tids, $vid_from, $relate_to_tids, $vid_to) {
  foreach ($relate_from_tids as $tid) {
    // get relations from all vocabularies
    $related_tids = array_keys(term_relations_get_related($tid, 'tid'));
    // toggle operation so if any in relate to are already related - remove relationship.
    $tids_to_remove = array_intersect($related_tids, $relate_to_tids);
    $related_updated_tids = array_diff(array_merge($related_tids, $relate_to_tids), $tids_to_remove);
    term_relations_set_related($tid, $related_updated_tids);
  }
}
